package com.fruitcomments.api.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="fruits")
public class Fruit {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	@Column
	private String name;
	@Column
	private String description;
	@Column
	private String photoUrl;
	
	@Transient
	@OneToMany(cascade=CascadeType.ALL,mappedBy="fruit")
	@JoinColumn(name="fruit_id")
	private List<Comment> comment;
	
	public Fruit() {
		super();
	}
	
	public Fruit(int id, String name, String description, String photoUrl) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.photoUrl = photoUrl;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPhotoUrl() {
		return photoUrl;
	}
	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}

	@Override
	public String toString() {
		return "Fruit [id=" + id + ", name=" + name + ", description=" + description + ", photoUrl=" + photoUrl + "]";
	}
	
	
	
}
