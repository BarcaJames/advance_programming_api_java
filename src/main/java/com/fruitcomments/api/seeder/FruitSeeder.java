package com.fruitcomments.api.seeder;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Component;

import com.fruitcomments.api.entities.Fruit;
import com.fruitcomments.api.repositories.IFruitRepository;

@Component
public class FruitSeeder {
	@Autowired
	private IFruitRepository fruitRepo;
	
	@EventListener
	public void seed(ContextRefreshedEvent ev) {
		Fruit[] fruits = new Fruit[] {
			new Fruit(1, "Orange", "It is orange", "orange.png"),	
			new Fruit(2, "Strawberry", "Seeds on the outside", "strawberry.png"),
		};
		System.out.println("--------- Seeding -------");
		
		for(Fruit defaultFruit : fruits) {
			Example<Fruit> fruitExample = Example.of(defaultFruit);
			Optional<Fruit> fruitResult = fruitRepo.findOne(fruitExample);
			
			if(! fruitResult.isPresent()) {
				fruitRepo.save(defaultFruit);
				System.out.println("Added fruit - "+ defaultFruit);
			}else {
				System.out.println("Fruit already added" + defaultFruit);
			}
		}
		
		System.out.println("--------- Seeding Complete -------");
	}
}
